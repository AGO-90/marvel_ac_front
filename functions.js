
function searchHeroByID(){
  heroID = document.getElementById("hero").value;
  fetch("http://localhost:8080/api/v1/marvel/getHeroByID/"+heroID)
  .then(response => response.json())
  .then(hero => showHeroDetails(hero));
}

showHeroDetails = hero => {
    const inputHeroID = document.querySelector('#heroID_response');
    const inputHeroName = document.querySelector('#heroName_response');
    const inputHeroDescription = document.querySelector('#heroDescription_response');
    const tableBody = document.querySelector('#tableBody');
    tableBody.innerHTML = "";
    hero.comics.forEach(comic => {
      const comicTR = document.createElement("tr");
      const comicTD = document.createElement("td");
      var text = document.createTextNode(comic);
      comicTD.appendChild(text);
      comicTR.appendChild(comicTD);
      tableBody.appendChild(comicTR);
    });
    inputHeroID.value = hero.id;
    inputHeroName.value = hero.name;
    inputHeroDescription.value = hero.description;
  }